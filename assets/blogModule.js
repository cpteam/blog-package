$(document).ready(function(){
    var $body = $('body');
    window.blogModule = {};
    window.blogModule.tagPopup = null;
    if($("#blog-markdown-article-text").length) {
        var simplemdeArticleText = new SimpleMDE({ element: $("#blog-markdown-article-text")[0]});
    }
    if($("#blog-markdown-article-perex").length) {
        var simplemdeArticlePerex = new SimpleMDE({ element: $("#blog-markdown-article-perex")[0]});
    }

    // $body.on('click', '.cpteam_bm_image_container', function () {
    //     if(typeof window.myClipboard === 'undefined' || !window.myClipboard) {
    //         window.myClipboard = new Clipboard('.cpteam_bm_image_container');
    //         $(this).trigger('click');
    //     }
    // });

    $body.on('click', '#mrceperka_gallery', function () {
        if(typeof window.myClipboard === 'undefined' || !window.myClipboard) {
            window.myClipboard = new Clipboard('.image');
            $(this).trigger('click');
        }
    });

    $body.on('click', '.bm_popup_open', function () {
        var $this = $(this);
        var targetId = $this.data('target');
        var $target = $(targetId);
        $target.removeClass('bm_display--none');

        if(window.blogModule.tagPopup !== null) {
            window.blogModule.tagPopup.addClass('bm_display--none');
        }
        window.blogModule.tagPopup = $target;

        var totalHeight = 0;
        $target.children().each(function(i, j){
            totalHeight += $(j).outerHeight(true);
        });

        $target.css({
            height: Math.max(100, totalHeight) + 50
        });
    });

    $body.on('click', '.bm_popup_close', function () {
        var $this = $(this);
        $this.closest('.bm_popup_target').addClass('bm_display--none');
        window.blogModule.tagPopup = null;
    });

    $body.on('change', 'form.stateSwitcher.ajax input', function () {
        $(this).closest('form').submit();
    });


});

