<?php
namespace CPTeam\Packages\BlogPackage\FrontModule\Presenters;


use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\IArticleDetailControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\IArticleRosterControlFactory;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use Nette\Application\BadRequestException;

class ArticlePresenter extends AFrontPresenter
{
	public $article;
	
	/** @var IArticleRepository @inject */
	public $storage;
	
	/** @var  IArticleRosterControlFactory @inject */
	public $articleRosterControlFactory;
	
	/** @var  IArticleDetailControlFactory @inject */
	public $articleDetailControlFactory;
	
	public function createComponentArticleRoster()
	{
		return $this->articleRosterControlFactory->create();
	}
	
	public function actionDefault($id, $slug)
	{
		$this->article = $this->storage->getArticleById($id);
		if(!$this->article) {
			throw new BadRequestException('Article does not exist', 404);
		}
	}
	
	public function createComponentArticleDetail()
	{
		$adc = $this->articleDetailControlFactory->create();
		$adc->setArticle($this->article);
		return $adc;
	}
}