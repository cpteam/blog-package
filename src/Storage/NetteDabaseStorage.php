<?php
namespace CPTeam\Packages\BlogPackage\Storage;

use CPTeam\Image\Saver\ImageSaver;
use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Article;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use CPTeam\Packages\BlogPackage\Storage\Repository\IImageRepository;
use CPTeam\Packages\BlogPackage\Storage\Repository\ITagRepository;
use CPTeam\Packages\BlogPackage\Storage\Repository\IUserRepository;
use Nette\Database\IRow;
use Nette\Database\Table\ActiveRow;
use Nette\Http\Session;

/**
 * Class NetteDatabaseStorage
 * @package CPTeam\Packages\BlogPackage\Storage
 */
final class NetteDatabaseStorage implements
	IArticleRepository,
	IUserRepository,
	ITagRepository,
	IImageRepository,
	IStorage
{
	/** @var  \Nette\Database\Context */
	private $db;
	
	/** @var  Config */
	private $config;
	
	/** @var  ImageSaver */
	private $imageSaver;
	
	/** @var  Session */
	private $session;
	
	private $articleTable;
	private $userTable;
	private $revisionTable;
	private $tagTable;
	private $imageTable;
	
	private $article_x_tagTable;
	private $article_x_imageTable;
	private $article_x_userTable;
	
	/**
	 * NetteDatabaseStorage constructor.
	 * @param \Nette\Database\Context $db
	 * @param Config $config
	 * @param ImageSaver $imageSaver
	 * @param Session $session
	 */
	public function __construct(\Nette\Database\Context $db, Config $config, ImageSaver $imageSaver, Session $session)
	{
		$this->db = $db;
		$this->config = $config;
		$this->imageSaver = $imageSaver;
		$this->session = $session;
		
		$this->init();
	}
	
	
	private function init()
	{
		$this->articleTable = $this->config->get('model:article:table');
		$this->userTable = $this->config->get('model:user:table');
		$this->revisionTable = $this->config->get('model:revision:table');
		$this->tagTable = $this->config->get('model:tag:table');
		$this->imageTable = $this->config->get('model:image:table');
		
		$this->article_x_tagTable = $this->config->get('model:article_x_tag:table');
		$this->article_x_imageTable = $this->config->get('model:article_x_image:table');
		$this->article_x_userTable = $this->config->get('model:article_x_user:table');
	}
	
	//-------------------------------------------- article --------------------------------------------
	public function getAllArticles()
	{
		return $this->db->table($this->articleTable);
	}
	
	public function getReleasedArticles()
	{
		foreach ($this->db->table($this->articleTable)
					 ->where('deleted_at IS NULL AND state = ?', Article::STATE_RELEASED)
					 ->order('created_at DESC')
				 as $article) {
			yield $this->getAllForArticle($article);
		}
	}
	
	public function getAllForArticle($article)
	{
		return [
			'article' => $article,
			'creator' => $article->creator,
			'tags' => $this->getArticleTags($article),
			'authors' => $this->getArticleAuthors($article),
			'authorsCount' => $this->getArticleAuthorsCount($article)
		];
	}
	
	public function getArticles()
	{
		return $this->db->table($this->articleTable)->where('deleted_at IS NULL');
	}
	
	/**
	 * @param $article
	 * @return array
	 */
	public function getArticleTags($article)
	{
		$sql = 'SELECT * FROM ' . $this->tagTable . ' t'
			. ' JOIN ' . $this->article_x_tagTable . ' axt ON t.id = axt.tag_id'
			. ' WHERE axt.article_id = ? '
			. ' ORDER BY t.priority DESC';
		return $this->db->query($sql, $article->id);
	}
	
	public function getArticleTagsIds($article)
	{
		foreach($this->getArticleTags($article) as $tag) {
			yield $tag->id;
		}
	}
	
	public function getArticleAuthors($article)
	{
		foreach($article->related($this->article_x_userTable, 'article_id') as $axa) {
			yield $axa->user;
		}
	}
	
	protected function getArticleAuthorsCount($article)
	{
		return $article->related($this->article_x_userTable, 'article_id')->count();
	}
	
	/*
	public function getArticleCreator($article)
	{
		return $article->creator;
	}
	*/
	
	public function getArticleById($id)
	{
		return $this->db->table($this->articleTable)->get($id);
	}
	
	public function getArticleBySlug($slug)
	{
		return $this->db->table($this->articleTable)->where('slug = ?', $slug)->fetch();
	}
	
	public function getArticleImages($article)
	{
		foreach ($article->related($this->article_x_imageTable, 'article_id') as $axi) {
			yield $axi->image;
		}
	}
	
	/**
	 * @param ActiveRow $article
	 * @return mixed
	 */
	public function deleteArticle($article)
	{
		return $article->update(['deleted_at' => (new \DateTime())->format('Y-m-d H:i:s')]);
	}
	
	public function addArticle($values)
	{
		/** @var IBlogModuleUserEntityProvider $user */
		$user = $values['user'];
		
		//article
		$articleData = [
			'headline' => $values['headline'],
			'perex' => $values['perex'],
			'text' => $values['text'],
			'slug' => $values['slug'] ? $values['slug'] : \Nette\Utils\Strings::webalize($values['headline']),
			'state' => $values['state'],
			'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
			'creator_id' => $user->getBlogModuleUserEntity()->id
		];
		$article = $this->db->table($this->articleTable)->insert($articleData);
		
		//revision
		$revisionData = [
			'headline' => $values['headline'],
			'perex' => $values['perex'],
			'text' => $values['text'],
			'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
			'article_id' => $article->id,
			'creator_id' => $user->getBlogModuleUserEntity()->id
		];
		
		$this->db->table($this->revisionTable)->insert($revisionData);
		
		//tags
		if(!empty($values['tags'])) {
			$tagGenerator = function ($tags) use ($article) {
				foreach ($tags as $tag) {
					yield [
						'article_id' => $article->id,
						'tag_id' => $tag
					];
				}
			};
			
			$this->db->table($this->article_x_tagTable)->insert($tagGenerator($values['tags']));
		}

		//images
		$this->addArticleImages($article, $user);
		
	}
	
	
	/**
	 * @param ActiveRow $article
	 * @param $values
	 * @return ActiveRow
	 */
	public function editArticle($article, $values)
	{
		$articleData = [
			'headline' => $values['headline'],
			'perex' => $values['perex'],
			'text' => $values['text'],
			'state' => $values['state'],
			'slug' => $values['slug'],
			'updated_at' => (new \DateTime())->format('Y-m-d H:i:s')
		];
		
		
		//tags
		$this->mergeArticleTags($article, $values['tags']);
		
		//image
		$this->addArticleImages($article, $values['user']);
		
		$article->update($articleData);
		return $article;
	}
	
	public function editArticleState($article, $values)
	{
		return $article->update(['state' => $values['state']]);
	}
	
	private function addArticleImages($article, IBlogModuleUserEntityProvider $user)
	{
		$section = $this->session->getSection('images');
		if($section->paths) {
			$imageGenerator = function($paths) use ($user, $article) {
				foreach ($paths as $hash => $relativePath) {
					$imageData = [
						'hash' => $hash,
						'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
						'uploaded_by_id' => $user->getBlogModuleUserEntity()->id
					];
					
					yield [
						'article_id' => $article->id,
						'image_id' => $this->db->table($this->imageTable)->insert($imageData)
					];
				}
			};
			
			$this->db->table($this->article_x_imageTable)->insert($imageGenerator($section->paths));
			unset($section->paths);
		}
	}
	
	private function mergeArticleTags($article, $tagIds)
	{
		//empty new tags
		if(empty($tagIds)) {
			$this->db->query('DELETE FROM ' . $this->article_x_tagTable . ' WHERE article_id = ?', $article->id);
			return;
		}
		
		
		//check if article has tags
		$empty = true;
		foreach($this->getArticleTags($article) as $tag) {
			$empty = false;
		}
		
		//article has no tags
		if($empty) {
			$tagInsertGenerator = function ($tagIds) use ($article) {
				foreach ($tagIds as $tagId) {
					yield [
						'article_id' => $article->id,
						'tag_id' => $tagId
					];
				}
			};
			
			$this->db->table($this->article_x_tagTable)->insert($tagInsertGenerator($tagIds));
			
		} else {
			$tagData = [];
			$tagData['delete'] = [];
			$tagData['insert'] = [];
			
			//reindex article tags
			$articleTags = [];
			foreach($this->getArticleTags($article) as $tag) {
				$articleTags[$tag->id] = $tag;
			}
			
			//old tags - new tags
			foreach($articleTags as $tag) {
				foreach($tagIds as $tagId) {
					if($tag->id === $tagId) break;
					else {
						$tagData['delete'][] = $tag->id;
					}
				}
			}
			
			//new tags - old tags
			foreach($tagIds as $tagId) {
				foreach($articleTags as $tag) {
					if($tag->id === $tagId) break;
					else {
						//if is not already in tags
						if(!isset($articleTags[$tagId])) {
							$tagData['insert'][] = [
								'article_id' => $article->id,
								'tag_id' => $tagId
							];
						}
					}
				}
			}
			
			if(!empty($tagData['delete'])) {
				$this->db->query('DELETE FROM ' . $this->article_x_tagTable . ' WHERE article_id = ? AND tag_id IN (?)', $article->id, $tagData['delete']);
			}
			if(!empty($tagData['insert'])) {
				$this->db->table($this->article_x_tagTable)->insert($tagData['insert']);
			}
		}
	}
	
	//-------------------------------------------- user --------------------------------------------
	public function getUserById($id)
	{
		return $this->db->table($this->userTable)->get($id);
	}
	
	//-------------------------------------------- tag --------------------------------------------
	public function getAllTags()
	{
		return $this->db->table($this->tagTable);
	}
	
	public function getTags()
	{
		return $this->db->table($this->tagTable)->where('deleted_at IS NULL');
	}
	
	/**
	 * @param $id
	 * @return IRow|false
	 */
	public function getTagById($id)
	{
		return $this->db->table($this->tagTable)->get($id);
	}
	
	/**
	 * @param $tag
	 * @return mixed
	 * @throws EDeletingForbidden
	 */
	public function deleteTag($tag)
	{
		if($tag->related($this->article_x_tagTable, 'tag_id')->count() === 0) {
			return $tag->update(['deleted_at' => (new \DateTime())->format('Y-m-d H:i:s')]);
		} else {
			throw new EDeletingForbidden('Can not delete tag which is used in some article');
		}
		
	}
	
	public function addTag($values)
	{
		/** @var IBlogModuleUserEntityProvider $user */
		$user = $values['user'];
		$tagData = [
			'name' => $values->name,
			'priority' => $values->priority,
			'creator_id' => $user->getBlogModuleUserEntity()->id,
			'created_at' => (new \DateTime())->format('Y-m-d H:i:s')
		];
		
		return $this->db->table($this->tagTable)->insert($tagData);
	}
	
	public function getTagPairs($key, $value)
	{
		$result = [];
		/** @var ActiveRow $item */
		foreach ($this->db->table($this->tagTable)->where('deleted_at IS NULL') as $item) {
			if ($item->offsetExists($key) && $item->offsetExists($value)) {
				$result[$item->{$key}] = $item->{$value};
			} else {
				throw new \LogicException('Property ' . $key . ' or ' . $value . ' does not exist in table ' . $this->tagTable);
			}
		}
		return $result;
	}
	
	/**
	 * @param ActiveRow $tag
	 * @param $values
	 * @return mixed
	 */
	public function editTag($tag, $values)
	{
		$tagData = [
			'name' => $values->name,
			'priority' => $values->priority,
		];
		
		$tag->update($tagData);
		return $tag;
	}
	
	//-------------------------------------------- images --------------------------------------------
	public function getImages()
	{
		return $this->db->table($this->imageTable)->where('deleted_at IS NULL');
	}
	
	public function getAllImages()
	{
		return $this->db->table($this->imageTable);
	}
}