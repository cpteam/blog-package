<?php
namespace CPTeam\Packages\BlogPackage\Storage\Repository;

interface ITagRepository
{
	public function getTags();
	public function getAllTags();
	public function getTagById($id);
	public function deleteTag($tag);
	public function addTag($values);
	public function editTag($tag, $values);
	public function getTagPairs($key, $value);
	
}