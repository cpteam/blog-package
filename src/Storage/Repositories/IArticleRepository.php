<?php
namespace CPTeam\Packages\BlogPackage\Storage\Repository;

interface IArticleRepository
{
	public function getArticles();
	public function getAllArticles();
	public function getArticleById($id);
	public function getArticleBySlug($slug);
	
	//public function getArticleCreator($article);
	
	public function getArticleTags($article);
	public function getArticleTagsIds($article);
	
	public function getArticleAuthors($article);
	public function getReleasedArticles();
	public function getAllForArticle($article);
	
	public function getArticleImages($article);
	
	public function addArticle($values);
	public function editArticle($article, $values);
	public function editArticleState($article, $values);
	public function deleteArticle($article);
}