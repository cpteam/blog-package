<?php
namespace CPTeam\Packages\BlogPackage\Storage\Repository;

interface IUserRepository
{
	public function getUserById($id);
}