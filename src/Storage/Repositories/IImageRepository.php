<?php
namespace CPTeam\Packages\BlogPackage\Storage\Repository;

interface IImageRepository
{
	public function getImages();
	
}