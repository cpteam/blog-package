<?php
namespace CPTeam\Packages\BlogPackage\Admin\Presenters;

use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Add\IArticleAddControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Edit\IArticleEditControlFactory;
use CPTeam\Packages\BlogPackage\Storage\IStorage;

class ArticlePresenter extends ABackPresenter
{
	public $article;
	
	/** @var  IStorage @inject */
	public $storage;
	
	/** @var  IArticleAddControlFactory @inject */
	public $articleAddControlFactory;
	
	/** @var  IArticleEditControlFactory @inject */
	public $articleEditControlFactory;
	
	
	public function createComponentArticleAdd()
	{
		return $this->articleAddControlFactory->create();
	}
	
	public function actionAdd()
	{
		
	}
	
	public function actionEdit($id)
	{
		$this->article = $this->storage->getArticleById($id);
	}
	
	public function createComponentArticleEdit()
	{
		$acf = $this->articleEditControlFactory->create();
		$acf->setArticle($this->article);
		return $acf;
	}
}