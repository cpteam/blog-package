<?php
namespace CPTeam\Packages\BlogPackage\Admin\Presenters;

use CPTeam\Image\Saver\ImageSaver;
use CPTeam\Packages\BlogPackage\Storage\IStorage;

class ApiPresenter extends ABackPresenter
{
	/** @var  IStorage @inject */
	public $storage;
	
	public function startup()
	{
		parent::startup();
		header('Access-Control-Allow-Origin: *');
	}
	
	public function actionGetImages($id)
	{
		if(is_numeric($id)) {
			$data = [];
			$article = $this->storage->getArticleById($id);
			if ($article) {
				$data = $this->imagesJson($this->storage->getArticleImages($article));
			}
			$this->sendJson($data);
			$this->terminate();
		}
		
		$this->sendJson($this->imagesJson($this->storage->getImages()->order('created_at DESC')));
		$this->terminate();
		
	}
	
	private function imagesJson($images) {
		$data = [];
		foreach ($images as $image) {
			$src = '//' . $this->config->get('domain') . (ImageSaver::getSrcRelative($this->config->get('images:dir'), $this->context->parameters['wwwDir'], $image->hash));
			$data[] = array_merge($image->toArray(), ['src' => $src, 'name' => $image->hash]);
		}
		return $data;
	}
}