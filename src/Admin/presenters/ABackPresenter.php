<?php
namespace CPTeam\Packages\BlogPackage\Admin\Presenters;

use CPTeam\Packages\Admin\Presenters\PackagePresenter;
use CPTeam\Packages\BlogPackage\Config;

abstract class ABackPresenter extends PackagePresenter
{
	/** @var  Config @inject */
	public $config;
	
	public function startup()
	{
		parent::startup();
		
		if (array_intersect($this->user->getRoles(), $this->config->get('access:allowed')) === []) {
			$message = $this->config->get('access:denied:message');
			if ($message['flash'] !== false) {
				$this->flashMessage($message['text'], $message['type']);
			}
			$this->redirect($this->config->get('access:denied:link'));
		}
	}
}