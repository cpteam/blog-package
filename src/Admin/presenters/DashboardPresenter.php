<?php
namespace CPTeam\Packages\BlogPackage\Admin\Presenters;

use CPTeam\Packages\BlogPackage\Components\Control\Back\Dashboard\IDashboardControlFactory;

class DashboardPresenter extends ABackPresenter
{
	
	/** @var  IDashboardControlFactory @inject */
	public $dashboardControlFactory;
	
	
	public function createComponentDashboard()
	{
		return $this->dashboardControlFactory->create();
	}
}