<?php

namespace CPTeam\Packages\BlogPackage\Traits;

trait TWithArticle
{
	/** @var   */
	protected $article;
	
	/**
	 * @return mixed
	 */
	public function getArticle()
	{
		return $this->article;
	}
	
	/**
	 * @param mixed $article
	 */
	public function setArticle($article)
	{
		$this->article = $article;
	}
	

	
	
}