<?php
namespace CPTeam\Packages\BlogPackage;

class Config
{
	/** @var array */
	private $data;
	
	/**
	 * Config constructor.
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}
	
	public function __get($key)
	{
		return $this->get($key);
	}
	
	public function get($key)
	{
		if(is_string($key) === false) {
			return null;
		}
		$keys = explode(':', $key);
		$keysCount = count($keys);
		
		if($keysCount === 0) {
			return null;
		}
		
		$current = null;
		if(isset($this->data[$keys[0]])) {
			$current = $this->data[$keys[0]];
		} else {
			return $current;
		}
		
		$i = 1;
		while($i < $keysCount) {
			if(isset($current[$keys[$i]])) {
				$current = $current[$keys[$i]];
				$i++;
			} else {
				break;
			}
		}
		
		return $current;
	}
	
	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
	
	
}
//
//$cfg = [
//	'back' => [
//		'links' => [],
//		'redirects' => [
//			'article' => [
//				'created' => 'AA',
//				'edited' => 'BB'
//			]
//		]
//	]
//];
//$cs = new Config($cfg);
