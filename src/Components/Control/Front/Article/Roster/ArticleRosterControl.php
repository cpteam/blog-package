<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail;

use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;

class ArticleRosterControl extends Control
{
	/** @var  IArticleRepository */
	protected $storage;
	
	/** @var  IArticleDetailControlFactory */
	protected $articleDetailControlFactory;
	
	/**
	 * ArticleRosterControl constructor.
	 * @param IArticleRepository $storage
	 * @param IArticleDetailControlFactory $articleDetailControlFactory
	 */
	public function __construct(IArticleRepository $storage, IArticleDetailControlFactory $articleDetailControlFactory)
	{
		$this->storage = $storage;
		$this->articleDetailControlFactory = $articleDetailControlFactory;
	}
	
	public function render()
	{
		$this->template->articleData = $this->storage->getReleasedArticles();
		$this->template->setFile(__DIR__ . '/articleRoster.latte');
		$this->template->render();
	}
	
	public function createComponentArticleDetail()
	{
		return new Multiplier(function($id) {
			$adc = $this->articleDetailControlFactory->create();
			$adc->setArticle($this->storage->getArticleById($id));
			$adc->setInRoster(true);
			return $adc;
		});
	}
}

interface IArticleRosterControlFactory
{
	/**
	 * @return ArticleRosterControl
	 */
	public function create();
}