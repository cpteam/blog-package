<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail;

use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use CPTeam\Packages\BlogPackage\Traits\TWithArticle;
use Nette\Application\UI\Control;

class ArticleDetailControl extends Control
{
	use TWithArticle;
	
	/** @var  Config */
	protected $config;
	
	/** @var  IArticleRepository */
	protected $storage;
	
	/** @var bool  */
	protected $inRoster = false;
	
	/**
	 * ArticleDetailControl constructor.
	 * @param Config $config
	 * @param IArticleRepository $storage
	 */
	public function __construct(Config $config, IArticleRepository $storage)
	{
		$this->config = $config;
		$this->storage = $storage;
	}
	
	
	public function render()
	{
		$this->template->data = $this->storage->getAllForArticle($this->article);
		$this->template->setFile(__DIR__ . '/articleDetail.latte');
		$this->template->render();
	}
	
	
	/**
	 * @return boolean
	 */
	public function isInRoster()
	{
		return $this->inRoster === true;
	}
	
	/**
	 * @param boolean $inRoster
	 */
	public function setInRoster($inRoster)
	{
		$this->inRoster = $inRoster;
	}
	
	/**
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->config;
	}
	
	
}

interface IArticleDetailControlFactory
{
	/**
	 * @return ArticleDetailControl
	 */
	public function create();
}