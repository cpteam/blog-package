<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Dashboard;

use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\StateSwitcher\IStateSwitcherControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Tag\ITagControlFactory;
use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use CPTeam\Packages\BlogPackage\Storage\IStorage;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;

class DashboardControl extends Control
{
	/** @var  Config */
	protected $config;
	
	/** @var  IStorage */
	protected $storage;
	
	/** @var  IBlogModuleUserEntityProvider */
	protected $user;
	
	/** @var ITagControlFactory */
	protected $tagControlFactory;
	
	/** @var  IStateSwitcherControlFactory */
	protected $stateSwitcherControlFactory;
	
	/**
	 * DashboardControl constructor.
	 * @param Config $config
	 * @param IStorage $storage
	 * @param IBlogModuleUserEntityProvider $user
	 * @param ITagControlFactory $tagControlFactory
	 * @param IStateSwitcherControlFactory $stateSwitcherControlFactory
	 */
	public function __construct(Config $config, IStorage $storage, IBlogModuleUserEntityProvider $user, ITagControlFactory $tagControlFactory, IStateSwitcherControlFactory $stateSwitcherControlFactory)
	{
		$this->config = $config;
		$this->storage = $storage;
		$this->user = $user;
		$this->tagControlFactory = $tagControlFactory;
		$this->stateSwitcherControlFactory = $stateSwitcherControlFactory;
	}
	
	
	public function render()
	{
		$this->template->articles = $this->storage->getArticles();
		$this->template->setFile(__DIR__ . '/dashboard.latte');
		$this->template->render();
	}
	
	/**
	 * @secured
	 * @param $id
	 */
	public function handleDelete($id)
	{
		$article = $this->storage->getArticleById($id);
		if ($article !== false) {
			$this->storage->deleteArticle($article);
			if ($this->presenter->isAjax()) {
				$this->redrawControl('articleListSnippet');
			} else {
				$this->redirect('this');
			}
		} else {
			$this->presenter->flashMessage('Článek nemohl být smazán, protože neexistuje');
			$this->redirect('this');
		}
		
	}
	
	public function createComponentTagControl()
	{
		return $this->tagControlFactory->create();
	}
	
	public function createComponentStateSwitcher()
	{
		return new Multiplier(function($articleId) {
			$article = $this->storage->getArticleById($articleId);
			
			$ssc = $this->stateSwitcherControlFactory->create();
			$ssc->setArticle($article);
			
			return $ssc;
		});
	}
	
	/**
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->config;
	}
	
}

interface IDashboardControlFactory
{
	/**
	 * @return DashboardControl
	 */
	public function create();
}