<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Tag;

use CPTeam\Packages\BlogPackage\Components\Form\TagFormFactory;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use CPTeam\Packages\BlogPackage\Storage\EDeletingForbidden;
use CPTeam\Packages\BlogPackage\Storage\IStorage;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;

class TagControl extends Control
{
	/** @var  IStorage */
	private $storage;
	
	/** @var  IBlogModuleUserEntityProvider */
	private $user;
	
	/** @var  TagFormFactory */
	private $tagFormFactory;
	
	/**
	 * DashboardControl constructor.
	 * @param IStorage $storage
	 * @param TagFormFactory $tagFormFactory
	 * @param IBlogModuleUserEntityProvider $user
	 */
	public function __construct(IStorage $storage, TagFormFactory $tagFormFactory, IBlogModuleUserEntityProvider $user)
	{
		$this->storage = $storage;
		$this->user = $user;
		$this->tagFormFactory = $tagFormFactory;
	}
	
	
	public function render()
	{
		$this->template->tags = $this->storage->getTags();
		$this->template->setFile(__DIR__ . '/tag.latte');
		$this->template->render();
	}
	
	public function createComponentAddTag()
	{
		$form = $this->tagFormFactory->create();
		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues();
			$values['user'] = $this->user;
			$this->storage->addTag($values);
			
			if($this->getPresenter()->isAjax()) {
				$this->redrawControl('tagListSnippet');
			} else {
				$this->redirect('this');
			}
		};
		
		return $form;
	}
	
	public function createComponentEditTag()
	{
		return new Multiplier(function($tagId) {
			$form = $this->tagFormFactory->create();
			$form['submit']->caption = 'Uložit';
			
			$tag = $this->storage->getTagById($tagId);
			$form->setDefaults([
				'name' => $tag->name,
				'priority' => $tag->priority
			]);
			
			$form->onSuccess[] = function(Form $form) use($tag) {
				$this->storage->editTag($tag, $form->getValues());
				
				if($this->getPresenter()->isAjax()) {
					$this->redrawControl('tagListSnippet');
				} else {
					$this->redirect('this');
				}
			};
			return $form;
		});
		
	}
	
	/**
	 * @secured
	 */
	public function handleDelete($id)
	{
		
		$tag = $this->storage->getTagById($id);
		if ($tag !== false) {
			try {
				$this->storage->deleteTag($tag);
			} catch (EDeletingForbidden $e) {
				$this->getPresenter()->flashMessage('Nelze smazat tag, který je použit v nějakém z článků');
			}
			
			if ($this->getPresenter()->isAjax()) {
				$this->redrawControl('tagListSnippet');
			} else {
				$this->redirect('this');
			}
		} else {
			$this->getPresenter()->flashMessage('Tag nemohl být smazán, protože neexistuje');
			$this->redirect('this');
		}
		
	}
	
}

interface ITagControlFactory
{
	/**
	 * @return TagControl
	 */
	public function create();
}