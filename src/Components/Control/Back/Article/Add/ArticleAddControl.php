<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Add;

use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Image\IArticleImageControlFactory;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleFormFactory;
use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use CPTeam\Packages\BlogPackage\Storage\IStorage;
use CPTeam\Packages\BlogPackage\Traits\TWithConfig;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Http\Session;


class ArticleAddControl extends Control
{
	/** @var  Config */
	protected $config;
	
	/** @var  IStorage */
	protected $storage;
	
	/** @var  Session */
	protected $session;
	
	/** @var  ArticleFormFactory */
	protected $articleFormFactory;
	
	/** @var  IBlogModuleUserEntityProvider */
	protected $user;
	
	/** @var  IArticleImageControlFactory */
	protected $articleImageControlFactory;
	
	/**
	 * ArticleAddControl constructor.
	 * @param Config $config
	 * @param IStorage $storage
	 * @param Session $session
	 * @param ArticleFormFactory $articleFormFactory
	 * @param IBlogModuleUserEntityProvider $user
	 * @param IArticleImageControlFactory $articleImageControlFactory
	 */
	public function __construct(Config $config, IStorage $storage, Session $session, ArticleFormFactory $articleFormFactory, IBlogModuleUserEntityProvider $user, IArticleImageControlFactory $articleImageControlFactory)
	{
		$this->config = $config;
		$this->storage = $storage;
		$this->session = $session;
		$this->articleFormFactory = $articleFormFactory;
		$this->user = $user;
		$this->articleImageControlFactory = $articleImageControlFactory;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/add.latte');
		$this->template->render();
	}
	
	public function createComponentAddArticle()
	{
		$form = $this->articleFormFactory->create();
		$form['tags']->setItems($this->storage->getTagPairs('id', 'name'));
		$form->onValidate[] = function (Form $form) {
			$values = $form->getValues();
			if(empty($values->perex)) {
				$form['perex']->addError('Vyplň perex prosím');
			}
			if(empty($values->text)) {
				$form['text']->addError('Vyplň obsah prosím');
			}
		};
		
		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);
			
			$values['user'] = $this->user;
			$this->storage->addArticle($values);
			$this->getPresenter()->redirect($this->config->get('back:redirects:article:created'));
		};
		
		return $form;
	}
	
	public function createComponentArticleImage()
	{
		return $this->articleImageControlFactory->create();
	}
}

interface IArticleAddControlFactory
{
	/**
	 * @return ArticleAddControl
	 */
	public function create();
}