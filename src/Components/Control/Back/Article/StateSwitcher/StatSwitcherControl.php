<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Article\StateSwitcher;

use CPTeam\Packages\BlogPackage\Components\Form\ArticleStateSwitcherFormFactory;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class StateSwitcherControl extends Control
{
	/** @var */
	private $article;
	
	/** @var  ArticleStateSwitcherFormFactory */
	private $stateSwitcherFormFactory;
	
	/** @var  IArticleRepository */
	private $storage;
	
	/**
	 * StateSwitcherControl constructor.
	 * @param ArticleStateSwitcherFormFactory $stateSwitcherFormFactory
	 * @param IArticleRepository $storage
	 */
	public function __construct(ArticleStateSwitcherFormFactory $stateSwitcherFormFactory, IArticleRepository $storage)
	{
		$this->stateSwitcherFormFactory = $stateSwitcherFormFactory;
		$this->storage = $storage;
	}
	
	
	public function render()
	{
		$this->template->article = $this->article;
		$this->template->setFile(__DIR__ . '/stateSwitcher.latte');
		$this->template->render();
	}
	
	/**
	 * @param mixed $article
	 */
	public function setArticle($article)
	{
		$this->article = $article;
	}
	
	public function createComponentStateSwitcher()
	{
		$form = $this->stateSwitcherFormFactory->create();
		$form['state']->setDefaultValue($this->article->state);
		
		$form->onSuccess[] = function (Form $form) {
			$this->storage->editArticleState($this->article, $form->getValues(true));
			if($this->getPresenter()->isAjax()) {
				$this->redrawControl('articleListSnippet');
			} else {
				$this->redirect('this');
			}
		};
		
		return $form;
	}
}

interface IStateSwitcherControlFactory
{
	/**
	 * @return StateSwitcherControl
	 */
	public function create();
}