<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Image;

use CPTeam\Image\Saver\ImageSaver;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleImageFormFactory;
use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Storage\IStorage;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use CPTeam\Packages\BlogPackage\Storage\Repository\IImageRepository;
use CPTeam\Packages\BlogPackage\Traits\TWithArticle;
use Nette\Application\UI\Form;
use Nette\Http\FileUpload;
use Nette\Http\Session;

class ArticleImageControl extends \Nette\Application\UI\Control
{
	use TWithArticle;
	
	/** @var  Config */
	protected $config;
	
	/** @var  IStorage|IArticleRepository|IImageRepository */
	protected $storage;
	
	/** @var  ImageSaver */
	protected $imageSaver;
	
	/** @var  ArticleImageFormFactory */
	protected $articleImageFormFactory;
	
	/** @var  Session */
	protected $session;
	
	/** @var \Nette\Http\SessionSection */
	protected $section;
	
	/** @var  string */
	private $wwwDir;
	
	/**
	 * ArticleImageControl constructor.
	 * @param Config $config
	 * @param IStorage|IArticleRepository|IImageRepository $storage
	 * @param ImageSaver $imageSaver
	 * @param ArticleImageFormFactory $articleImageFormFactory
	 * @param Session $session
	 */
	public function __construct(Config $config, IStorage $storage, ImageSaver $imageSaver, ArticleImageFormFactory $articleImageFormFactory, Session $session)
	{
		$this->config = $config;
		$this->storage = $storage;
		$this->imageSaver = $imageSaver;
		$this->articleImageFormFactory = $articleImageFormFactory;
		$this->session = $session;
		
		$this->section = $this->session->getSection('images');
	}
	
	
	public function render()
	{
		$this->wwwDir = $this->getPresenter()->context->getParameters()['wwwDir'];
		if(!$this->getPresenter()->isAjax()) {
			$uploadedImages = $this->article ? $this->storage->getArticleImages($this->article) : $this->storage->getImages();
			$this->template->uploadedImages = [];
			foreach($uploadedImages as $image) {
				$this->template->uploadedImages[] = [
					'image' => $image,
					'src' => '//' . $this->config->get('domain') . (ImageSaver::getSrcRelative($this->config->get('images:dir'), $this->wwwDir, $image->hash))
				];
			}
		}
		
		$this->template->fetchUrl = $this->presenter->link($this->config->get('api:get:images')) . ($this->article? '/' . $this->article->id : '');
		$this->template->domain = $this->config->get('domain');
		$this->template->images = $this->section->paths ?: [];
		$this->template->setFile(__DIR__ . '/image.latte');
		$this->template->render();
	}
	
	public function createComponentAddImage()
	{
		$form = $this->articleImageFormFactory->create();
		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues();
			
			//------------------------------------------
			/** @var FileUpload $image */
			foreach ($values->images as $image) {
				$result = $this->imageSaver->saveToDir($this->config->get('images:dir'), $image);
				$this->section->paths[$result->getBasename()] = $result->getRelativePath();
			}
			//------------------------------------------
			
			if($this->presenter->isAjax()) {
				$this->redrawControl('articleImages');
			} else {
				$this->redirect('this');
			}
			
		};
		
		return $form;
	}
	
	
	/**
	 * @secured
	 * @param $hash
	 */
	public function handleDelete($hash)
	{
		$file = ImageSaver::getSrc($this->config->get('images:dir'), $hash);
		if(file_exists($file) && is_writeable($file)) {
			unlink($file);
			unset($this->session->getSection('images')->paths[$hash]);
		} else {
			$this->getPresenter()->flashMessage('Cannot delete');
		}
		
		if($this->presenter->isAjax()) {
			$this->redrawControl('articleImages');
		} else {
			$this->redirect('this');
		}
	}
	
	/**
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->config;
	}
}

interface IArticleImageControlFactory
{
	/**
	 * @return ArticleImageControl
	 */
	public function create();
}