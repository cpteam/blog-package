<?php
namespace CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Edit;

use CPTeam\Image\Saver\ImageSaver;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Image\IArticleImageControlFactory;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleFormFactory;
use CPTeam\Packages\BlogPackage\Config;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use CPTeam\Packages\BlogPackage\Storage\Repository\IArticleRepository;
use CPTeam\Packages\BlogPackage\Traits\TWithArticle;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ArticleEditControl extends Control
{
	use TWithArticle;
	
	/** @var  Config */
	protected $config;
	
	/** @var  IArticleRepository */
	protected $storage;
	
	/** @var  ArticleFormFactory */
	protected $articleFormFactory;
	
	/** @var  IBlogModuleUserEntityProvider */
	protected $user;
	
	/** @var  ImageSaver */
	protected $imageSaver;
	
	/** @var  IArticleImageControlFactory */
	protected $articleImageControlFactory;
	
	/**
	 * ArticleEditControl constructor.
	 * @param Config $config
	 * @param IArticleRepository $storage
	 * @param ArticleFormFactory $articleFormFactory
	 * @param IBlogModuleUserEntityProvider $user
	 * @param ImageSaver $imageSaver
	 * @param IArticleImageControlFactory $articleImageControlFactory
	 */
	public function __construct(Config $config, IArticleRepository $storage, ArticleFormFactory $articleFormFactory, IBlogModuleUserEntityProvider $user, ImageSaver $imageSaver, IArticleImageControlFactory $articleImageControlFactory)
	{
		$this->config = $config;
		$this->storage = $storage;
		$this->articleFormFactory = $articleFormFactory;
		$this->user = $user;
		$this->imageSaver = $imageSaver;
		$this->articleImageControlFactory = $articleImageControlFactory;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/edit.latte');
		$this->template->render();
	}
	
	public function createComponentEditArticle()
	{
		$form = $this->articleFormFactory->create();

		$form['tags']->setItems($this->storage->getTagPairs('id', 'name'));
		$form['tags']->setDefaultValue(iterator_to_array($this->storage->getArticleTagsIds($this->article)));
		
		$form['state']->setDefaultValue($this->article->state);
		
		$form->setDefaults([
			'headline' => $this->article->headline,
			'perex' => $this->article->perex,
			'text' => $this->article->text,
			'slug' => $this->article->slug
		]);
		$form['submit']->caption = 'Uložit';
		
		$form->onValidate[] = function (Form $form) {
			$values = $form->getValues();
			if(empty($values->perex)) {
				$form['perex']->addError('Vyplň perex prosím');
			}
			if(empty($values->text)) {
				$form['text']->addError('Vyplň obsah prosím');
			}
		};
		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);
			
			$values['user'] = $this->user;
			$this->storage->editArticle($this->article, $values);
			
			$this->getPresenter()->redirect($this->config->get('back:redirects:article:edited'));
		};
		
		return $form;
	}
	
	public function createComponentArticleImage()
	{
		$aic = $this->articleImageControlFactory->create();
		$aic->setArticle($this->article);
		return $aic;
	}
}

interface IArticleEditControlFactory
{
	/**
	 * @return ArticleEditControl
	 */
	public function create();
}