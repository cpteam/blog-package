<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;

class ArticleImageFormFactory implements IFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * ArticleImageFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->elementPrototype->addAttributes(['class' => 'ajax']);
		$form->addMultiUpload('images', 'Obrázky');
		$form->addSubmit('submit', 'Nahrát');
		
		return $form;
	}
}