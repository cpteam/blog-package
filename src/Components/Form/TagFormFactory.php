<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;

class TagFormFactory implements IFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * ArticleImageFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->elementPrototype->addAttributes([
			'class' => 'ajax',
			'formnovalidate' => true,
		]);
		$form->addText('name', 'Název')
			->setRequired('Prosím, vyplňte název tagu');
		
		$form->addText('priority', 'Priorita')
			->setType('number')
			->setDefaultValue(1)
			->setRequired('Prosím, vyplňte prioritu tagu');
		
		$form->addSubmit('submit', 'Přidat');
		
		return $form;
	}
}