<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;


use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Article;
use Nette\Forms\Form;

class ArticleFormFactory implements IFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * ArticleImageFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		
		$form->elementPrototype->addAttributes(['class' => 'markdown']);
		
		$form->addText('headline', 'Nadpis')
			->addRule(Form::MAX_LENGTH, 'Maximální délka je 250 znaků', 250)
			->setRequired('Vyplňte nadpis prosím');
		$form->addText('slug', 'Název URL');
		
		$form->addTextArea('perex', 'Perex')
			->addRule(Form::MAX_LENGTH, 'Maximální délka je 250 znaků', 250)
			->setRequired(false) //but validate after
			->controlPrototype->addAttributes(['id' => 'blog-markdown-article-perex']);
		
		$form->addTextArea('text', 'Obsah')
			->setRequired(false) //but validate after
			->controlPrototype->addAttributes(['id' => 'blog-markdown-article-text']);

		$form->addMultiSelect('tags', 'Tagy');
		
		$form->addSelect('state', 'Stav',
			[
				Article::STATE_CONCEPT => 'Koncept',
				Article::STATE_NEEDS_APPROVAL => 'Potřebuje schválit',
				Article::STATE_RELEASED => 'Vydaný'
			]
		)
			->setDefaultValue(Article::STATE_RELEASED);
		
		$form->addSubmit('submit', 'Vytvořit');
		return $form;
	}
}