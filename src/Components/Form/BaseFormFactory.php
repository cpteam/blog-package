<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;


use Nette\Forms\IFormRenderer;
use Nette\Localization\ITranslator;

class BaseFormFactory implements IFormFactory
{
	/** @var  IFormRenderer */
	private $renderer = null;
	
	/** @var  ITranslator */
	private $translator = null;
	
	/**
	 * @param IFormRenderer $renderer
	 */
	public function setRenderer(IFormRenderer $renderer)
	{
		$this->renderer = $renderer;
	}
	
	/**
	 * @param ITranslator $translator
	 */
	public function setTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}
	

	
	public function create()
	{
		$form = new \Nette\Application\UI\Form();
		
		if($this->renderer !== null) {
			$form->setRenderer($this->renderer);
		}
		
		if($this->translator !== null) {
			$form->setTranslator($this->translator);
		}
		
		return $form;
	}
}