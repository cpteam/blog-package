<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;

interface IFormFactory
{
	public function create();
}