<?php
namespace CPTeam\Packages\BlogPackage\Components\Form;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Article;

class ArticleStateSwitcherFormFactory implements IFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * ArticleImageFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->elementPrototype->addAttributes(['class' => 'ajax stateSwitcher']);
		$form->addRadioList('state', 'Stav', [
			Article::STATE_CONCEPT => 'Koncept',
			Article::STATE_NEEDS_APPROVAL => 'Potřebuje schválit',
			Article::STATE_RELEASED => 'Vydaný'
		]);
		$form->addSubmit('submit', 'Uložit'); //predelat na onchange
		
		return $form;
	}
}