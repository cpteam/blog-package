<?php
namespace CPTeam\Packages\BlogPackage;

use CPTeam\Packages\BlogPackage\Storage\NetteDatabaseStorage;
use CPTeam\Packages\Package;
use Nette\Forms\Rendering\DefaultFormRenderer;

/**
 * Class BlogPackage
 *
 * @package CPTeam\Packages\TestPackage
 */
class BlogPackage extends Package
{
	protected $defaults = [
		'model' => [
			'article' => [
				'table' => 'module_blog_article',
			],
			'user' => [
				'table' => 'user',
			],
			'revision' => [
				'table' => 'module_blog_revision',
			],
			'tag' => [
				'table' => 'module_blog_tag',
			],
			'article_x_tag' => [
				'table' => 'module_blog_article_x_module_blog_tag',
			],
			'image' => [
				'table' => 'module_blog_image',
			],
			'article_x_image' => [
				'table' => 'module_blog_article_x_module_blog_image',
			],
			'article_x_user' => [
				'table' => 'module_blog_article_x_user',
			]
		],
		'access' => [
			'allowed' => [],
			'denied' => [
				'message' => [
					'type' => 'warn',
					'text' => 'Přístup zamítnut',
					'flash' => true
				],
				'link' => '', //OUTER ONLY
			]
		],
		'storage' => NetteDatabaseStorage::class,
		'images' => [
			'dir' => '', //OUTER ONLY
		],
		'gallery' => [
			'bundle' => '/assets/blog/gallery/bundle.js',
		],
		'domain' => '', //OUTER ONLY
		'front' => [
			'layout' => '', //OUTER ONLY
			'links' => [
				'article' => [
					'detail' => '', //OUTER ONLY
				],
				'user' => [
					'detail' => '', //OUTER ONLY
				],
			],
		],
		'back' => [
			'layout' => '', //OUTER ONLY
			'redirects' => [
				'article' => [
					'created' => 'this',
					'edited' => ':AdminPackage:Blog:Dashboard:default', //DEFAULT
				],
			],
			'links' => [
				'article' => [
					'preview' => '', //OUTER ONLY
					'edit' => ':AdminPackage:Blog:Article:edit', //DEFAULT
					'add' => ':AdminPackage:Blog:Article:add', //DEFAULT
				],
			],
		],
		'api' => [
			'get' => [
				'images' => ':AdminPackage:Blog:Api:getImages', //DEFAULT
			]
		],
		'formRenderer' => DefaultFormRenderer::class,
	];
	
	public $description = "Blog package";
	
	public $presenter = "Dashboard";
	
}