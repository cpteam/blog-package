<?php
namespace CPTeam\Packages\BlogPackage;


use CPTeam\Image\Saver\ImageSaver;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Add\ArticleAddControl;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Add\IArticleAddControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Edit\ArticleEditControl;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Edit\IArticleEditControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Image\ArticleImageControl;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\Image\IArticleImageControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\StateSwitcher\IStateSwitcherControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Article\StateSwitcher\StateSwitcherControl;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Dashboard\DashboardControl;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Dashboard\IDashboardControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Tag\ITagControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Back\Tag\TagControl;
use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\ArticleDetailControl;
use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\ArticleRosterControl;
use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\IArticleDetailControlFactory;
use CPTeam\Packages\BlogPackage\Components\Control\Front\Article\Detail\IArticleRosterControlFactory;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleFormFactory;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleImageFormFactory;
use CPTeam\Packages\BlogPackage\Components\Form\ArticleStateSwitcherFormFactory;
use CPTeam\Packages\BlogPackage\Components\Form\BaseFormFactory;
use CPTeam\Packages\BlogPackage\Components\Form\TagFormFactory;
use CPTeam\Packages\TestPackage\PackageInstallator;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Forms\IFormRenderer;


class BlogPackageInstallator extends PackageInstallator
{
	public function install(array $config)
	{
		$config = $this->checkConfig($config);
		$builder = $this->builder;
		if($builder->getByType(\Nette\Database\Context::class) === null) {
			throw new \LogicException('Please, use ' . \Nette\Database\Context::class . ' for blogModule');
		}
		
		if($builder->getByType(ImageSaver::class) === null) {
			throw new \LogicException('Please, register ' . ImageSaver::class . 'extension for blogModule');
		}
		//config
		$builder->addDefinition($this->prefix('config'))
			->setClass(Config::class)
			->setArguments([$config]);
		
		//storage
		$builder->addDefinition($this->prefix('storage'))
			->setClass($config['storage']);
		
		//base form
		$baseForm = $builder->addDefinition($this->prefix('baseFormFormFactory'))
			->setClass(BaseFormFactory::class);
		if(in_array(IFormRenderer::class, class_implements($config['formRenderer']))) {
			$baseForm->addSetup('setRenderer', [new $config['formRenderer']]);
		} else {
			throw new \LogicException('Form renderer has to implement ' . IFormRenderer::class);
		}
		
		//back
		//article - ff
		$builder->addDefinition($this->prefix('articleFormFactory'))
			->setClass(ArticleFormFactory::class);
		
		//atricle - add - control
		$builder->addDefinition($this->prefix('articleAddControlFactory'))
			->setClass(ArticleAddControl::class)
			->setImplement(IArticleAddControlFactory::class);
		
		//article - edit - control
		$builder->addDefinition($this->prefix('articleEditControlFactory'))
			->setClass(ArticleEditControl::class)
			->setImplement(IArticleEditControlFactory::class);
		
		//article - image - ff
		$builder->addDefinition($this->prefix('articleImageFormFactory'))
			->setClass(ArticleImageFormFactory::class);
		
		//article - image - control
		$builder->addDefinition($this->prefix('articleImageControlFactory'))
			->setClass(ArticleImageControl::class)
			->setImplement(IArticleImageControlFactory::class);
		
		//tag - ff
		$builder->addDefinition($this->prefix('tagFormFactory'))
			->setClass(TagFormFactory::class);
		
		//article - edit - control
		$builder->addDefinition($this->prefix('tag'))
			->setClass(TagControl::class)
			->setImplement(ITagControlFactory::class);
		
		//article - stateSwitcher - control
		$builder->addDefinition($this->prefix('articleStateSwitcherControlFactory'))
			->setClass(StateSwitcherControl::class)
			->setImplement(IStateSwitcherControlFactory::class);
		
		$builder->addDefinition($this->prefix('articleStateSwitcherFormFactory'))
			->setClass(ArticleStateSwitcherFormFactory::class);
		
		
		
		//dashboard - control
		$builder->addDefinition($this->prefix('dashboardControlFactory'))
			->setClass(DashboardControl::class)
			->setImplement(IDashboardControlFactory::class);
		
		//front
		//list - control
		$builder->addDefinition($this->prefix('articleListFront'))
			->setClass(ArticleRosterControl::class)
			->setImplement(IArticleRosterControlFactory::class);
		
		//list - detail
		$builder->addDefinition($this->prefix('articleDetailFront'))
			->setClass(ArticleDetailControl::class)
			->setImplement(IArticleDetailControlFactory::class);
		
		
		//latte - macro
		$latteFactory = $builder->getDefinition($builder->getByType(ILatteFactory::class));
		$latteFactory
			//copy pasted from latte extension
			->addSetup('?->onCompile[] = function ($engine) { ' . \CPTeam\Bridges\Nette\Latte\Macros\MarkdownMacros::class . '::install($engine->getCompiler()); }', ['@self']);
	}
	
	/**
	 * @param $config
	 */
	private function checkConfig($config)
	{
		if ($config['access']['allowed'] === []) {
			throw new \LogicException('Set config[access][allowed] key in config');
		}
		if ($config['domain'] === '') {
			throw new \LogicException('Set config[domain] key in config');
		}
		if ($config['images']['dir'] === '') {
			throw new \LogicException('Set config[images][dir] key in config');
		}
		if ($config['front']['layout'] === '') {
			throw new \LogicException('Set config[front][layout] key in config');
		}
		if ($config['back']['layout'] === '') {
			throw new \LogicException('Set config[front][layout] key in config');
		}
		if ($config['front']['links']['user']['detail'] === '') {
			throw new \LogicException('Set config[front][links][user][detail] key in config');
		}
		return $config;
	}
}