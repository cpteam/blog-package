<?php
namespace CPTeam\Bridges\Nette\Security;
use CPTeam\Packages\BlogPackage\Storage\Repository\IUserRepository;

trait TUser
{
	/** @var  IUserRepository */
	private $userRepo;
	
	public function setBlogStorage(IUserRepository $userStorage)
	{
		$this->userRepo = $userStorage;
	}
	
	public function getBlogModuleUserEntity()
	{
		if ($this->isLoggedIn() == false) {
			return NULL;
		}
		return $this->userRepo->getUserById($this->getId());
	}
}