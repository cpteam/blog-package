<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Traits;

trait TIdentifier
{
	/** @var  integer */
	private $id;
	
	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	
	
	
}