<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Traits;

trait TCreatedDeleted
{
	/** @var  \DateTime */
	private $createdAt;
	
	/** @var  \DateTime */
	private $deletedAt;
	
	/**
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}
	
	/**
	 * @param \DateTime $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getDeletedAt()
	{
		return $this->deletedAt;
	}
	
	/**
	 * @param \DateTime $deletedAt
	 */
	public function setDeletedAt($deletedAt)
	{
		$this->deletedAt = $deletedAt;
	}
}