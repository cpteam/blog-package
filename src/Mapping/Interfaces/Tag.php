<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface Tag
{
	public function getName();
	public function getPriority();
	public function getCreator();
}