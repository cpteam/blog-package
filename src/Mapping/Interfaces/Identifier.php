<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface Identifier
{
	public function getId();
}