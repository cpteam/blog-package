<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface User
{
	public function getEmail();
}