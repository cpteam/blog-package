<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface Image
{
	public function getHash();
	public function getUploader();
}