<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface Revision
{
	public function getArticle();
	public function getCreator();
	
	public function getPerex();
	public function getHeadline();
	public function getText();
}