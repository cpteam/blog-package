<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface Article
{
	const STATE_CONCEPT = 'concept';
	const STATE_NEEDS_APPROVAL = 'needs_aproval';
	const STATE_RELEASED = 'released';
	
	public function getAuthors();
	public function getCreator();
	public function getTags();
	
	public function getPerex();
	public function getHeadline();
	public function getText();
	public function getSlug();
	public function getState();
	
	public function setAuthors($authors);
	public function setCreator(User $creator);
	public function setTags($tags);
}