<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface CreatedDeleted
{
	public function getCreatedAt();
	public function getDeletedAt();
}