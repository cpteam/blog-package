<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Interfaces;
interface FromArray
{
	public function fromArray($data);
}