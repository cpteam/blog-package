<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Entity;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Article as IArticle;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\CreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\FromArray;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Identifier;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Tag;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\User;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TCreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TIdentifier;

class Article implements IArticle, Identifier, CreatedDeleted, FromArray
{
	use TIdentifier;
	use TCreatedDeleted;
	
	/** @var  User */
	private $creator;
	
	/** @var Tag[] */
	private $tags;
	
	/** @var array  */
	private $authors = [];
	
	/** @var  string */
	private $perex;
	
	/** @var string */
	private $headline;
	
	/** @var  string */
	private $text;
	
	/** @var  string */
	private $slug;
	
	/** @var  string */
	private $state;
	
		
	public function fromArray($data)
	{
		$this->id = $data['id'];
		$this->headline = $data['headline'];
		$this->slug = $data['slug'];
		$this->perex = $data['perex'];
		$this->text = $data['text'];
		$this->state = $data['state'];
		$this->createdAt = $data['created_at'];
		$this->deletedAt = $data['deleted_at'];
	}
	
	public function toArray()
	{
		return [
			'id' => $this->id,
			'headline' => $this->headline,
			'slug' => $this->slug,
			'perex' => $this->perex,
			'text' => $this->text,
			'state' => $this->state,
			'created_at' => $this->createdAt,
			'deleted_at' => $this->deletedAt,
			'creator_id' => $this->creator->getId()
		];
	}
	
	/**
	 * @return User
	 */
	public function getCreator()
	{
		return $this->creator;
	}
	
	/**
	 * @param User $creator
	 */
	public function setCreator(User $creator)
	{
		$this->creator = $creator;
	}
	
	/**
	 * @return \CPTeam\Packages\BlogPackage\Mapping\Interfaces\Tag[]
	 */
	public function getTags()
	{
		return $this->tags;
	}
	
	/**
	 * @param \CPTeam\Packages\BlogPackage\Mapping\Interfaces\Tag[] $tags
	 */
	public function setTags($tags)
	{
		$this->tags = $tags;
	}
	
	/**
	 * @return array
	 */
	public function getAuthors()
	{
		return $this->authors;
	}
	
	/**
	 * @param array $authors
	 */
	public function setAuthors($authors)
	{
		$this->authors = $authors;
	}
	
	/**
	 * @return string
	 */
	public function getPerex()
	{
		return $this->perex;
	}
	
	/**
	 * @param string $perex
	 */
	public function setPerex($perex)
	{
		$this->perex = $perex;
	}
	
	/**
	 * @return string
	 */
	public function getHeadline()
	{
		return $this->headline;
	}
	
	/**
	 * @param string $headline
	 */
	public function setHeadline($headline)
	{
		$this->headline = $headline;
	}
	
	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}
	
	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}
	
	/**
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}
	
	/**
	 * @param string $slug
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;
	}
	
	/**
	 * @return string
	 */
	public function getState()
	{
		return $this->state;
	}
	
	/**
	 * @param string $state
	 */
	public function setState($state)
	{
		$this->state = $state;
	}
	
	

	
	
	
	
}