<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Entity;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\CreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\FromArray;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Identifier;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Tag as ITag;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TCreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TIdentifier;

class Tag implements ITag, Identifier, CreatedDeleted, FromArray
{
	use TIdentifier;
	use TCreatedDeleted;
	
	/** @var  string */
	private $name;
	
	/** @var  integer */
	private $priority;
	
	/** @var  User */
	private $creator;
	
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * @return integer
	 */
	public function getPriority()
	{
		return $this->priority;
	}
	
	/**
	 * @param integer $priority
	 */
	public function setPriority($priority)
	{
		$this->priority = $priority;
	}
	
	/**
	 * @return User
	 */
	public function getCreator()
	{
		return $this->creator;
	}
	
	/**
	 * @param User $creator
	 */
	public function setCreator($creator)
	{
		$this->creator = $creator;
	}
	
	
	
	public function fromArray($data)
	{
		$this->id = $data['id'];
		$this->name = $data['name'];
		$this->priority = $data['priority'];
		$this->createdAt = new \DateTime($data['created_at']);
		$this->deletedAt = new \DateTime($data['deleted_at']);
	}
	
	
}