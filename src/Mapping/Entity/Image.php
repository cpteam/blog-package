<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Entity;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\CreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\FromArray;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Identifier;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Image as IImage;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\User;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TCreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TIdentifier;

class Image implements IImage, Identifier, CreatedDeleted, FromArray
{
	use TIdentifier;
	use TCreatedDeleted;
	
	/** @var  string */
	private $hash;
	
	/** @var  User */
	private $uploader;
	
	public function fromArray($data)
	{
		$this->id = $data['id'];
		$this->hash = $data['hash'];
		$this->createdAt = new \DateTime($data['created_at']);
		$this->deletedAt = new \DateTime($data['deleted_at']);
	}
	
	/**
	 * @return string
	 */
	public function getHash()
	{
		return $this->hash;
	}
	
	/**
	 * @param string $hash
	 */
	public function setHash($hash)
	{
		$this->hash = $hash;
	}
	
	/**
	 * @return User
	 */
	public function getUploader()
	{
		return $this->uploader;
	}
	
	/**
	 * @param User $uploader
	 */
	public function setUploader($uploader)
	{
		$this->uploader = $uploader;
	}
	
	
	
	
}