<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Entity;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Article;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\CreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\FromArray;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Identifier;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Revision as IRevision;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\User;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TCreatedDeleted;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TIdentifier;

class Revision implements IRevision, Identifier, CreatedDeleted, FromArray
{
	use TIdentifier;
	use TCreatedDeleted;
	
	/** @var  User */
	private $creator;
	
	/** @var  string */
	private $perex;
	
	/** @var string */
	private $headline;
	
	/** @var  string */
	private $text;
	
	/** @var  Article */
	private $article;
	
	public function fromArray($data)
	{
		$this->id = $data['id'];
		$this->headline = $data['headline'];
		$this->perex = $data['perex'];
		$this->text = $data['text'];
		$this->createdAt = new \DateTime($data['created_at']);
		$this->deletedAt = new \DateTime($data['deleted_at']);
	}
	
	/**
	 * @return User
	 */
	public function getCreator()
	{
		return $this->creator;
	}
	
	/**
	 * @param User $creator
	 */
	public function setCreator($creator)
	{
		$this->creator = $creator;
	}
	
	/**
	 * @return string
	 */
	public function getPerex()
	{
		return $this->perex;
	}
	
	/**
	 * @param string $perex
	 */
	public function setPerex($perex)
	{
		$this->perex = $perex;
	}
	
	/**
	 * @return string
	 */
	public function getHeadline()
	{
		return $this->headline;
	}
	
	/**
	 * @param string $headline
	 */
	public function setHeadline($headline)
	{
		$this->headline = $headline;
	}
	
	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}
	
	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}
	
	/**
	 * @return Article
	 */
	public function getArticle()
	{
		return $this->article;
	}
	
	/**
	 * @param Article $article
	 */
	public function setArticle($article)
	{
		$this->article = $article;
	}
	
	
	
	
	
}