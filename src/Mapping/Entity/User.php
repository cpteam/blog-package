<?php
namespace CPTeam\Packages\BlogPackage\Mapping\Entity;

use CPTeam\Packages\BlogPackage\Mapping\Interfaces\FromArray;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\Identifier;
use CPTeam\Packages\BlogPackage\Mapping\Interfaces\User as IUser;
use CPTeam\Packages\BlogPackage\Mapping\Traits\TIdentifier;

class User implements IUser, Identifier, FromArray
{
	use TIdentifier;
	
	/** @var  string */
	private $email;
	
	
	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}
	
	public function fromArray($data)
	{
		$this->id = $data['id'];
		$this->email = $data['email'];
	}
	
	
	
	
}