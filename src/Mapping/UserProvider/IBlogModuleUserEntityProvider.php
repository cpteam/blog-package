<?php
namespace CPTeam\Packages\BlogPackage\Mapping;
use CPTeam\Packages\BlogPackage\Storage\Repository\IUserRepository;

interface IBlogModuleUserEntityProvider
{
	public function getBlogModuleUserEntity();
	public function setBlogStorage(IUserRepository $blogStorage);
}